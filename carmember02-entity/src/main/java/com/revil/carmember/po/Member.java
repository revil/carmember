package com.revil.carmember.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @description:
 * @author: guoly
 * @Date: 2021-10-27 17:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_member")
@ApiModel(value = "Member对象", description = "")
public class Member implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;

    private String loginacct;

    private String userpswd;

    private String username;

    private String email;

    @ApiModelProperty(value = "实名认证状态0 - 未实名认证， 1 - 实名认证申请中， 2 - 已实名认证")
    private Integer authstatus;

    @ApiModelProperty(value = " 0 - 个人， 1 - 企业")
    private Integer usertype;

    private String realname;

    private String cardnum;

    @ApiModelProperty(value = "0 - 企业， 1 - 个体， 2 - 个人， 3 - 政府")
    private Integer accttype;


}
