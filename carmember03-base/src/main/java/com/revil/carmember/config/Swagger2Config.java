package com.revil.carmember.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @description:
 * @author: guoly
 * @Date: 2021-10-27 16:18
 */
@EnableSwagger2
@Component
public class Swagger2Config {

    @Bean
    public Docket getAdminApiConfig() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("AdminApi").apiInfo(getAdminApiInfo())
                .select().paths(Predicates.and(PathSelectors.regex("/admin/.*"))).build();
    }

    private ApiInfo getAdminApiInfo() {
        return new ApiInfoBuilder().title("后台管理系统API文档").description("本文档描述了后台管理系统各API接口的调用方式")
                .version("v1.0").contact(new Contact("Revil", "", "Revil@qq.com")).build();
    }

    @Bean
    public Docket getWebApiConfig() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("WebApi")
                .select().paths(Predicates.and(PathSelectors.regex("/api/.*"))).build();
    }

}
