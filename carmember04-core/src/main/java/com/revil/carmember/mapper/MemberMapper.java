package com.revil.carmember.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.revil.carmember.po.Member;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Revil
 * @since 2021-10-27
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {

}
