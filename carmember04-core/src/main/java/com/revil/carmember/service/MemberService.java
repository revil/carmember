package com.revil.carmember.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.revil.carmember.po.Member;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Revil
 * @since 2021-10-27
 */
public interface MemberService extends IService<Member> {

}
