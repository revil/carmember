package com.revil.carmember.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.revil.carmember.mapper.MemberMapper;
import com.revil.carmember.po.Member;
import com.revil.carmember.service.MemberService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Revil
 * @since 2021-10-27
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

}
