package com.revil.carmember.test;

import com.revil.carmember.mapper.MemberMapper;
import com.revil.carmember.po.Member;
import com.revil.carmember.service.MemberService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @description:
 * @author: guoly
 * @Date: 2021-10-28 9:58
 */
@SpringBootTest
public class MyBatisTest {

    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    @Autowired
    private MemberMapper mapper;

    @Autowired
    private MemberService memberService;

    @Test
    public void testConn() throws SQLException {
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
    }

    @Test
    public void list() {
        List<Member> memberList = mapper.selectList(null);
        System.out.println(memberList);

        List<Member> list = memberService.list();
        System.out.println(list);
    }

    @Test
    public void add() {
        Member member = new Member();
        member.setAuthstatus(0);
        member.setAccttype(2);
        member.setCardnum("020216");
        member.setEmail("revil@qq.com");
        member.setLoginacct("Revil");
        member.setRealname("Revil");
        member.setUsername("Revil");
        member.setUsertype(0);
        member.setUserpswd("666666");

        memberService.save(member);
    }

}
