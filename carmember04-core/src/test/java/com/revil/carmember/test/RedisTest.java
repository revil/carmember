package com.revil.carmember.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @description:
 * @author: guoly
 * @Date: 2021-10-28 14:19
 */
@SpringBootTest
public class RedisTest {

    @Autowired
    private StringRedisTemplate template;

    @Test
    public void test1() {
        template.opsForValue().set("天气", "晴朗");
        System.out.println("天气" + template.opsForValue().get("天气"));
    }

}
