package com.revil.carmember;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description:
 * @author: guoly
 * @Date: 2021-10-28 15:10
 */
@EnableDiscoveryClient
@SpringBootApplication
public class WebUIApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebUIApplication.class, args);
    }

}
