package com.revil.carmember.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description:
 * @author: guoly
 * @Date: 2021-10-28 15:28
 */

@Controller
public class PortalController {

    @RequestMapping("/")
    public String portal() {
        return "portal";
    }

}
